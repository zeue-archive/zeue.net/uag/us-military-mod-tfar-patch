class CfgPatches {
    class us_military_mod_tfar_patch {
        units[] = {
            "usm_pack_alice_prc119",
            "usm_pack_alice_prc77",
            "usm_pack_st138_prc77"
        };
        weapons[] = {};
        requiredVersion = 1.56;
        requiredAddons[] = {"us_military_units", "tfar_core"};
        author = "";
        authors[] = {"Unnamed Arma Group", "Cody (@Zeue)"};
        authorUrl = "https://uagpmc.com";
    };
};

class CfgVehicles {
    class usm_pack_alice_prc119;
    class usm_pack_alice_prc77;
    class usm_pack_st138_prc77;

    class usm_pack_alice_prc119 : usm_pack_alice_prc119 {
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
        tf_subtype = "airborne";
    }

    class usm_pack_alice_prc77 : usm_pack_alice_prc77 {
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
        tf_subtype = "airborne";
    }

    class usm_pack_st138_prc77 : usm_pack_st138_prc77 {
        tf_dialog = "anarc210_radio_dialog";
        tf_dialogUpdate = "[""CH%1""] call TFAR_fnc_updateLRDialogToChannel;";
        tf_encryptionCode = "tf_west_radio_code";
        tf_hasLRradio = 1;
        tf_range = 40000;
        tf_subtype = "airborne";
    }
}